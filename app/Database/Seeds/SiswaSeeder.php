<?php namespace App\Database\Seeds;

class SiswaSeeder extends \CodeIgniter\Database\Seeder
{
    public function run()
    {

        $kabupaten = [
            [
                'kabupaten' => 'Kota Bandung'
            ],
            [
                'kabupaten' => 'Kota Cimahi'
            ],
            [
                'kabupaten' => 'Kab. Bandung Barat'
            ]
        ];

        $kecamatan = [
            [
                'kecamatan' => 'Bandung Timur',
                'kabupaten_id' => 1
            ],
            [
                'kecamatan' => 'Cimahi Utara',
                'kabupaten_id' => 2
            ],
            [
                'kecamatan' => 'Padalarang',
                'kabupaten_id' => 3
            ],
            [
                'kecamatan' => 'Cimahi Tengah',
                'kabupaten_id' => 2
            ],
            [
                'kecamatan' => 'Antapani',
                'kabupaten_id' => 1
            ],
            [
                'kecamatan' => 'Lembang',
                'kabupaten_id' => 3
            ],
            [
                'kecamatan' => 'Cimahi Selatan',
                'kabupaten_id' => 2
            ],
            [
                'kecamatan' => 'Batujajar',
                'kabupaten_id' => 3
            ]
        ];

        $siswa = [
            [ 'nama' => 'Agus', 'kabupaten_id' => 1, 'kecamatan_id'=> 1, 'alamat' => 'Alamat Siswa' ],
            [ 'nama' => 'Budi', 'kabupaten_id' => 2, 'kecamatan_id'=> 2, 'alamat' => 'Alamat Siswa' ],
            [ 'nama' => 'Nana', 'kabupaten_id' => 3, 'kecamatan_id'=> 3, 'alamat' => 'Alamat Siswa' ],
            [ 'nama' => 'Bambang', 'kabupaten_id' => 3, 'kecamatan_id'=> 3, 'alamat' => 'Alamat Siswa' ],
            [ 'nama' => 'Fitri', 'kabupaten_id' => 2, 'kecamatan_id'=> 4, 'alamat' => 'Alamat Siswa' ],
            [ 'nama' => 'Bagus', 'kabupaten_id' => 1, 'kecamatan_id'=> 5, 'alamat' => 'Alamat Siswa' ],
            [ 'nama' => 'Hartoko', 'kabupaten_id' => 1, 'kecamatan_id'=> 5, 'alamat' => 'Alamat Siswa' ],
            [ 'nama' => 'Dadan', 'kabupaten_id' => 3, 'kecamatan_id'=> 6, 'alamat' => 'Alamat Siswa' ],
            [ 'nama' => 'Ceceng', 'kabupaten_id' => 2, 'kecamatan_id'=> 7, 'alamat' => 'Alamat Siswa' ],
            [ 'nama' => 'Ilham', 'kabupaten_id' => 1, 'kecamatan_id'=> 1, 'alamat' => 'Alamat Siswa' ],
            [ 'nama' => 'Iqbal', 'kabupaten_id' => 1, 'kecamatan_id'=> 8, 'alamat' => 'Alamat Siswa' ],
            [ 'nama' => 'Adi', 'kabupaten_id' => 2, 'kecamatan_id'=> 4, 'alamat' => 'Alamat Siswa' ],
        ];

        $this->db->table('kabupaten')->insertBatch($kabupaten);
        $this->db->table('kecamatan')->insertBatch($kecamatan);
        $this->db->table('siswa')->insertBatch($siswa);
    }
}