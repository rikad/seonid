<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class AddSiswa extends Migration
{
	public function up()
	{
		$this->forge->addField([
			'id' => [
				'type'           => 'INT',
				'unsigned'       => true,
				'auto_increment' => true,
			],
			'nama' => [
				'type'           => 'VARCHAR',
				'constraint'     => '100',
			],
			'kabupaten_id'=> [
				'type'           => 'INT',
				'unsigned'       => true,
			],
			'kecamatan_id' => [
				'type'           => 'INT',
				'unsigned'       => true,
			],
			'alamat' => [
				'type'           => 'VARCHAR',
				'constraint'     => '200',
				'null'           => true,
			],
		]);

		$this->forge->addKey('id', true);
		$this->forge->addForeignKey('kabupaten_id','kabupaten','id','RESTRICT','CASCADE');
		$this->forge->addForeignKey('kecamatan_id','kecamatan','id','RESTRICT','CASCADE');
		$this->forge->createTable('siswa');
	}

	//--------------------------------------------------------------------

	public function down()
	{
		$this->forge->dropTable('siswa');
	}
}
