<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class AddKabupaten extends Migration
{
	public function up()
	{
		$this->forge->addField([
			'id' => [
				'type'           => 'INT',
				'unsigned'       => true,
				'auto_increment' => true,
			],
			'kabupaten' => [
				'type'           => 'VARCHAR',
				'constraint'     => '100',
			]
		]);

		$this->forge->addKey('id', true);
		$this->forge->createTable('kabupaten');
	}

	//--------------------------------------------------------------------

	public function down()
	{
		$this->forge->dropTable('kabupaten');
	}
}