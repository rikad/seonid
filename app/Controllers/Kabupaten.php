<?php namespace App\Controllers;

use \App\Models\KabupatenModel;

class Kabupaten extends BaseController
{
	public function index()
	{
        $kabupaten = new KabupatenModel();
        $data = $kabupaten->findAll();

		return view('kabupaten/index', [
            'data' => $data
        ]);
	}

	public function create()
	{
		return view('kabupaten/form');
	}

	public function update($id)
	{
        $kabupaten = new KabupatenModel();
        $data = $kabupaten->find($id);

		return view('kabupaten/form', [
            'data' => $data
        ]);
	}

	public function store()
	{
		if ($this->request->getMethod() !== 'post') return false;

		$kabupaten = new KabupatenModel();

		$data = [
			'id' => $this->request->getPost('id'),
			'kabupaten' => $this->request->getPost('kabupaten')
		];

		if ($kabupaten->save($data) === false)
		{
			redirect()->back();
		}
		
		return redirect()->to('/kabupaten');
	}

	public function delete($id)
	{
        $kabupaten = new KabupatenModel();
        $data = $kabupaten->delete($id);

		return redirect()->to('/kabupaten');
	}
}
