<?php namespace App\Controllers;

use \App\Models\KecamatanModel;
use \App\Models\KabupatenModel;

class Kecamatan extends BaseController
{
	public function index()
	{
        $kecamatan = new KecamatanModel();
        $data = $kecamatan->select('kecamatan.*, kabupaten.kabupaten')
                ->join('kabupaten', 'kabupaten.id = kecamatan.kabupaten_id')
                ->findAll();

		return view('kecamatan/index', [
            'data' => $data
        ]);
    }

    public function create()
	{
        $kab = new KabupatenModel();
        $kab = $kab->findAll();

        return view('kecamatan/form', [
            'kab' => $kab
        ]);
	}

	public function update($id)
	{
        $kecamatan = new KecamatanModel();
        $data = $kecamatan->find($id);

        $kab = new KabupatenModel();
        $kab = $kab->findAll();

		return view('kecamatan/form', [
            'data' => $data,
            'kab' => $kab
        ]);
	}

	public function store()
	{
		if ($this->request->getMethod() !== 'post') return false;

		$kecamatan = new KecamatanModel();

		$data = [
			'id' => $this->request->getPost('id'),
			'kecamatan' => $this->request->getPost('kecamatan'),
			'kabupaten_id' => $this->request->getPost('kabupaten_id')
        ];

		if ($kecamatan->save($data) === false)
		{
            // return var_dump($kecamatan->errors());
			redirect()->back();
		}
		
		return redirect()->to('/kecamatan');
	}

	public function delete($id)
	{
        $kecamatan = new KecamatanModel();
        $data = $kecamatan->delete($id);

		return redirect()->to('/kecamatan');
	}
}
