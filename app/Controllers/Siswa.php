<?php namespace App\Controllers;

use \App\Models\SiswaModel;
use \App\Models\KabupatenModel;
use \App\Models\KecamatanModel;

class Siswa extends BaseController
{
	public function index()
	{
        $siswa = new SiswaModel();
        $data = $siswa->select('siswa.*, kabupaten.kabupaten, kecamatan.kecamatan')
                ->join('kabupaten', 'kabupaten.id = siswa.kabupaten_id')
                ->join('kecamatan', 'kecamatan.id = siswa.kecamatan_id')
                ->findAll();

		return view('siswa/index', [
            'data' => $data
        ]);
    }

    public function create()
	{
		$kab = new KabupatenModel();
		$kab = $kab->findAll();
		
		$kec = new KecamatanModel();
        $kec = $kec->findAll();

		return view('siswa/form', [
			'kab' => $kab,
			'kec' => $kec
		]);
	}

	public function update($id)
	{
        $siswa = new SiswaModel();
		$data = $siswa->find($id);
		
		$kab = new KabupatenModel();
        $kab = $kab->findAll();

		$kec = new KecamatanModel();
        $kec = $kec->findAll();

		return view('siswa/form', [
			'data' => $data,
			'kab' => $kab,
			'kec' => $kec
        ]);
	}

	public function store()
	{
		if ($this->request->getMethod() !== 'post') return false;

		$siswa = new SiswaModel();

		$data = [
			'id' => $this->request->getPost('id'),
			'nama' => $this->request->getPost('nama'),
			'kecamatan_id' => $this->request->getPost('kecamatan_id'),
			'kabupaten_id' => $this->request->getPost('kabupaten_id'),
			'alamat' => $this->request->getPost('alamat')
		];

		if ($siswa->save($data) === false)
		{
			return var_dump($siswa->errors());
			redirect()->back();
		}
		
		return redirect()->to('/siswa');
	}

	public function delete($id)
	{
        $siswa = new SiswaModel();
        $data = $siswa->delete($id);

		return redirect()->to('/siswa');
	}
}
