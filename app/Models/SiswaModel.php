<?php namespace App\Models;

use CodeIgniter\Model;

class SiswaModel extends Model
{
    protected $table = 'siswa';

    protected $allowedFields = ['nama', 'kabupaten_id', 'kecamatan_id', 'alamat'];

    protected $validationRules = [
        'nama'     => 'required',
        'kecamatan_id'     => 'required',
        'kabupaten_id'     => 'required',
        'alamat'     => 'required',
    ];

    protected $skipValidation = false;

}