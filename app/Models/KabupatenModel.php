<?php namespace App\Models;

use CodeIgniter\Model;

class KabupatenModel extends Model
{
    protected $table = 'kabupaten';

    protected $allowedFields = ['kabupaten'];

    protected $validationRules = [
        'kabupaten'     => 'required'
    ];

    protected $skipValidation = false;

}