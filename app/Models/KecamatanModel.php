<?php namespace App\Models;

use CodeIgniter\Model;

class KecamatanModel extends Model
{
    protected $table = 'kecamatan';

    protected $allowedFields = ['kecamatan', 'kabupaten_id'];

    protected $validationRules = [
        'kecamatan'     => 'required',
        'kabupaten_id'     => 'required'
    ];

    protected $skipValidation = false;

}