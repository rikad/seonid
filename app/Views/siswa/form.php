<?= $this->extend('layout/layout') ?>

<?= $this->section('content') ?>
<section>

	<h1>Kabupaten</h1>
    <hr>
    <?= \Config\Services::validation()->listErrors(); ?>

    <br>
    <div class="row">
        <div class="col-md-8" style="margin:auto">

            <form action="/siswa/store" method="post">

            <?= csrf_field() ?>

            <?php if(isset($data)): ?>
            <input type="hidden" class="form-control" placeholder="Kota Bandung" name="id" value="<?= isset($data) ? $data['id'] : '' ?>">
            <?php endif ?>

                <div class="form-group">
                    <label for="nama">Nama :</label>
                    <input type="nama" class="form-control" placeholder="Ujang" name="nama" value="<?= isset($data) ? $data['nama'] : '' ?>" required>
                </div>

                <div class="form-group">
                    <label for="kabupaten_id">Pilih Kabupaten:</label>
                    <select class="form-control" name="kabupaten_id" id="kabForm" onclick="genKec(this.value)" required>
                        <?php foreach($kab as $v): ?>
                            <option value="<?php echo $v['id']?>"><?php echo $v['kabupaten']?></option>
                        <?php endforeach ?>
                    </select>
                </div>

                <div class="form-group">
                    <label for="kecamatan_id">Pilih Kecamatan:</label>
                    <select class="form-control" name="kecamatan_id" id="kecForm" required>
                    </select>
                </div>

                <div class="form-group">
                    <label for="alamat">Alamat :</label>
                    <textarea class="form-control" name="alamat" required><?= isset($data) ? $data['alamat'] : '' ?></textarea>
                </div>

                <div align="right">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>

</section>

<script>
    var kec = JSON.parse(`<?= json_encode($kec) ?>`);
    
    function genKec(kab_id) {
        var html = '';

        kec.forEach(function(v, i) {
            if(kab_id == v.kabupaten_id) {
                html += `<option value="${v.id}">${v.kecamatan}</option>`
            }
        });

        document.querySelector('#kecForm').innerHTML = html;
    }

    genKec(document.querySelector('#kabForm').value);

</script>

<?= $this->endSection() ?>
