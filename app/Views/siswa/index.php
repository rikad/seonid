<?= $this->extend('layout/layout') ?>

<?= $this->section('content') ?>
<section>

	<h1>Siswa</h1>
    <hr>
	<p>
    <div align="right">
        <a href="/siswa/create" class="btn btn-primary btn-sm">Tambah</a>
    </div>
    <br>
        <table id="datatable" class="table table-striped table-bordered" style="width:100%">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Nama</th>
                    <th>Kota/Kabupaten</th>
                    <th>Kecamatan</th>
                    <th>Alamat</th>
                    <th></th>
                </tr>
            </thead>

            <tbody>
                <?php foreach($data as $v): ?>
                    <tr>
                        <td><?php echo $v['id'] ?></td>
                        <td><?php echo $v['nama'] ?></td>
                        <td><?php echo $v['kecamatan'] ?></td>
                        <td><?php echo $v['kabupaten'] ?></td>
                        <td><?php echo $v['alamat'] ?></td>
                        <td><a href="/siswa/update/<?php echo $v['id'] ?>" class="btn btn-warning btn-sm">Edit</a> <a href="/siswa/delete/<?php echo $v['id'] ?>" class="btn btn-danger btn-sm">Hapus</a></td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
	</p>

</section>
<?= $this->endSection() ?>
