<?= $this->extend('layout/layout') ?>

<?= $this->section('content') ?>
<section>

	<h1>Kabupaten</h1>
    <hr>
    <?= \Config\Services::validation()->listErrors(); ?>

    <br>
    <div class="row">
        <div class="col-md-8" style="margin:auto">

            <form action="/kabupaten/store" method="post">

            <?= csrf_field() ?>

            <?php if(isset($data)): ?>
            <input type="hidden" class="form-control" placeholder="Kota Bandung" name="id" value="<?= isset($data) ? $data['id'] : '' ?>">
            <?php endif ?>

                <div class="form-group">
                    <label for="kabupaten">Nama Kabupaten:</label>
                    <input type="kabupaten" class="form-control" placeholder="Kota Bandung" name="kabupaten" value="<?= isset($data) ? $data['kabupaten'] : '' ?>" required>
                </div>
                <div align="right">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>

</section>
<?= $this->endSection() ?>
