<?= $this->extend('layout/layout') ?>

<?= $this->section('content') ?>
<section>

	<h1>Kabupaten</h1>
    <hr>
    <?= \Config\Services::validation()->listErrors(); ?>

    <br>
    <div class="row">
        <div class="col-md-8" style="margin:auto">

            <form action="/kecamatan/store" method="post">

            <?= csrf_field() ?>

            <?php if(isset($data)): ?>
            <input type="hidden" class="form-control" placeholder="Kota Bandung" name="id" value="<?= isset($data) ? $data['id'] : '' ?>">
            <?php endif ?>

                <div class="form-group">
                    <label for="kecamatan">Nama Kecamatan:</label>
                    <input type="kecamatan" class="form-control" placeholder="Batujajar" name="kecamatan" value="<?= isset($data) ? $data['kecamatan'] : '' ?>" required>
                </div>
                <div class="form-group">
                    <label for="kabupaten_id">Pilih Kabupaten:</label>
                    <select class="form-control" name="kabupaten_id" required>
                        <?php foreach($kab as $v): ?>
                            <option value="<?php echo $v['id']?>"><?php echo $v['kabupaten']?></option>
                        <?php endforeach ?>
                    </select>
                </div>
                <div align="right">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>

</section>
<?= $this->endSection() ?>
