<?= $this->extend('layout/layout') ?>

<?= $this->section('content') ?>
<section>

	<h1>Kecamatan</h1>
    <hr>
	<p>
    <div align="right">
        <a href="/kecamatan/create" class="btn btn-primary btn-sm">Tambah</a>
    </div>
    <br>
        <table id="datatable" class="table table-striped table-bordered" style="width:100%">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Kecamatan</th>
                    <th>Kabupaten</th>
                    <th></th>
                </tr>
            </thead>

            <tbody>
                <?php foreach($data as $v): ?>
                    <tr>
                        <td><?php echo $v['id'] ?></td>
                        <td><?php echo $v['kecamatan'] ?></td>
                        <td><?php echo $v['kabupaten'] ?></td>
                        <td><a href="/kecamatan/update/<?php echo $v['id'] ?>" class="btn btn-warning btn-sm">Edit</a> <a href="/kecamatan/delete/<?php echo $v['id'] ?>" class="btn btn-danger btn-sm">Hapus</a></td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
	</p>

</section>
<?= $this->endSection() ?>
